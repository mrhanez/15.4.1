﻿#include <iostream>

// Функция для печати чётных или нечётных чисел
void printNumbers(int n, bool even) {
    // Определяем начальное значение и шаг для цикла
    int startValue = even ? 0 : 1;
    int step = 2;

    // Итерируемся от начального значения до n с шагом step
    for (int i = startValue; i <= n; i += step) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

int main() {
    const int N = 10; // Задаем константу N

    // Выводим все чётные числа от 0 до N
    for (int i = 0; i <= N; i += 2) {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    // Выводим все нечётные числа от 0 до N
    for (int i = 1; i <= N; i += 2) {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    // Используем функцию printNumbers для вывода чётных и нечётных чисел
    printNumbers(N, true); // Чётные числа
    printNumbers(N, false); // Нечётные числа

    return 0;
}
